package com.example.SpringBootMicroservicesExercises;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootMicroservicesExercisesApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootMicroservicesExercisesApplication.class, args);
    }

}
