package com.example.SpringBootMicroservicesExercises.Service;

import com.example.SpringBootMicroservicesExercises.Entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    List<Product> productList = new ArrayList<>();
    public List<Product> getProducts(){
        productList.add(new Product("Pants", "Jeans pants", 7.99));
        return productList;
    }

    public void addProduct(Product product){
        productList.add(product);
    }
}
