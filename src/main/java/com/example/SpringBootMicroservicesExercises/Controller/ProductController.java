package com.example.SpringBootMicroservicesExercises.Controller;

import com.example.SpringBootMicroservicesExercises.Entity.Product;
import com.example.SpringBootMicroservicesExercises.Service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    public ProductService productService;

    @GetMapping("/products")
    public ResponseEntity products() {
       return ResponseEntity.ok().body(productService.getProducts());
    }

    @GetMapping("/product/{id}")
    public ResponseEntity getProductById(@PathVariable Integer id) {
        return ResponseEntity.ok().body(productService.getProducts().get(id));
    }

    @PostMapping("/product")
    public void addProduct(@RequestBody Product product){
        productService.addProduct(product);

    }

    @DeleteMapping("/product/{id}")
    public void deleteProductById(@PathVariable Integer id) {

    }

    @PutMapping("/product")
    public void updateProduct(Product product) {

    }
}


