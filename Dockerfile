FROM openjdk:11-slim
COPY target/*.jar /home/app/springbootmicroservicesexercises.jar
ENTRYPOINT["java", "-jar", "/home/app/SpringBootMicroservicesExercises"]


#FROM openjdk:11-slim
#COPY target/*.jar /home/app/customerapi.jar
#ENTRYPOINT ["java", "-jar", "/home/app/customerapi.jar"]